import 'dart:io';
import 'Exit.dart';
import 'Player.dart';
import 'TableMap.dart';
import 'Zombies.dart';

void main(List<String> args) {
  showWelcome();
  showNewLine();
  showRule();
  showNewLine();
  showZombiesDirection();
  showNewLine();
  showStart();
  TableMap rooms = TableMap(5, 5);
  var player = Player(4, 0, "Y", rooms, 0);
  var z1 = Zombies(2, 2, "Z1");
  var z2 = Zombies(0, 3, "Z2");
  var z3 = Zombies(2, 3, "Z3");
  var z4 = Zombies(4, 1, "Z4");
  var z5 = Zombies(1, 2, "Z5");
  var exit = Exit(0, 4, "E");
  var countWalk = 0;
  var totalStep = 12;

  rooms.addObject(exit);
  rooms.setplayer(player);
  rooms.setZombie(z1);
  rooms.setZombie(z2);
  rooms.setZombie(z3);
  rooms.setZombie(z4);
  rooms.setZombie(z5);
  while (true) {
    
    String direction =" ";
    
    if (totalStep == 0) {
      print("You're are stuck in the maze");
      zombiesAttack();
      player.setPlayer("-");
      rooms.showMap();
      showLose();
      break;
    }

    if (rooms.escapeed == true) {
      player.setPlayer("-");
      printWin();
      break;
    }
    if (rooms.alive == false) {
      zombiesAttack();
      player.setPlayer("-");
      rooms.showMap();
      showLose();
      break;
    }
    rooms.showMap();
    showRemainStep(totalStep);
    direction = stdin.readLineSync()!;
    if (direction == "q") {
      print("Byeeeee!!");
      break;
    } 
      player.walk(direction);
      countWalk++;
      totalStep--;
    



    switch (countWalk) {
      case 1:
        z1.walk("w");
        z2.walk("a");
        z3.walk("a");
        z4.walk("w");
        z5.walk("a");
        break;
      case 2:
        z1.walk("w");
        z2.walk("a");
        z3.walk("s");
        z4.walk("w");
        z5.walk("a");
        break;
      case 3:
        z1.walk("a");
        z2.walk("a");
        z3.walk("d");
        z4.walk("w");
        z5.walk("d");
        break;
      case 4:
        z1.walk("a");
        z2.walk("s");
        z3.walk("w");
        z4.walk("w");
        z5.walk("d");
        break;
      case 5:
        z1.walk("s");
        z2.walk("d");
        z3.walk("a");
        z4.walk("s");
        z5.walk("d");
        break;
      case 6:
        z1.walk("s");
        z2.walk("s");
        z3.walk("s");
        z4.walk("s");
        z5.walk("d");
        break;
      case 7:
        z1.walk("s");
        z2.walk("d");
        z3.walk("d");
        z4.walk("s");
        z5.walk("a");
        break;
      case 8:
        z1.walk("s");
        z2.walk("d");
        z3.walk("w");
        z4.walk("s");
        z5.walk("a");
        break;
      case 9:
        z1.walk("d");
        z2.walk("d");
        z3.walk("a");
        z4.walk("w");
        z5.walk("a");
        break;
      case 10:
        z1.walk("d");
        z2.walk("w");
        z3.walk("s");
        z4.walk("w");
        z5.walk("a");
        break;
      case 11:
        z1.walk("w");
        z2.walk("a");
        z3.walk("d");
        z4.walk("w");
        z5.walk("d");
        break;
      case 12:
        z1.walk("w");
        z2.walk("a");
        z3.walk("w");
        z4.walk("w");
        z5.walk("d");
        break;
      default:
        break;
    }

    showNewLine();
      inGame();
  }
}

void showRemainStep(int totalStep) {
  print("Your step remain : $totalStep");
}

void printWin() {
      var dungeonMessage = new Runes('''
 \n 


██╗░░░██╗░█████╗░██╗░░░██╗  ░██╗░░░░░░░██╗██╗███╗░░██╗██╗██╗██╗
╚██╗░██╔╝██╔══██╗██║░░░██║  ░██║░░██╗░░██║██║████╗░██║██║██║██║
░╚████╔╝░██║░░██║██║░░░██║  ░╚██╗████╗██╔╝██║██╔██╗██║██║██║██║
░░╚██╔╝░░██║░░██║██║░░░██║  ░░████╔═████║░██║██║╚████║╚═╝╚═╝╚═╝
░░░██║░░░╚█████╔╝╚██████╔╝  ░░╚██╔╝░╚██╔╝░██║██║░╚███║██╗██╗██╗
░░░╚═╝░░░░╚════╝░░╚═════╝░  ░░░╚═╝░░░╚═╝░░╚═╝╚═╝░░╚══╝╚═╝╚═╝╚═╝
░░░╚═╝░░░░╚════╝░░╚═════╝░  ╚══════╝░╚════╝░╚═════╝░╚══════╝╚═╝╚═╝╚═╝
 ''');
   print(new String.fromCharCodes(dungeonMessage));
}

void showWelcome() {
  var dungeonMessage = new Runes('''
 \n 
░█▀▀▀█ ░█▀▀▀█ ░█▀▄▀█ ░█▀▀█ ▀█▀ ░█▀▀▀ ░█▀▀▀█ 　 ░█▀▄▀█ ─█▀▀█ ░█▀▀▀█ ░█▀▀▀ 
─▄▄▄▀▀ ░█──░█ ░█░█░█ ░█▀▀▄ ░█─ ░█▀▀▀ ─▀▀▀▄▄ 　 ░█░█░█ ░█▄▄█ ─▄▄▄▀▀ ░█▀▀▀ 
░█▄▄▄█ ░█▄▄▄█ ░█──░█ ░█▄▄█ ▄█▄ ░█▄▄▄ ░█▄▄▄█ 　 ░█──░█ ░█─░█ ░█▄▄▄█ ░█▄▄▄
 ''');
  print(new String.fromCharCodes(dungeonMessage));
  print("Welcome to Zombies Maze!!");
}

void showRule() {
  List<String> _rule = [];
  _rule = [
  "Rule : ",
  "1.walk into each room untill you reach the Exit(E).",
  "2.Everytime you walk the zombies(Z) will walk too. ",
  "3.If you meet the zombies(Z). THE GAME WILL END.",
  "4. You can walk 12 times at most. if you're still can't reach exit.You will LOSE."];
_runningDialog(_rule);
}

void showNewLine() {
  print("----------------------------------------------------------");
}

void showStart() {
  List<String> str = [];
  str =["Game start","Good luck"];
  _runningDialog(str);
}

void inGame() {
  print("You're still in the maze");
}

void showZombiesDirection() {
  
  print(" ------- ------- ------- ------- -------");
  print("|    ___|__^____|___    |       |  EXIT |");
  print("|   |  ||  |   _|___|___|_______|___Z2  |");
  print(" ---+--- --+--+- ---+--- ------- ----^--");
  print("|   |   |  |  | |   |   |       |    |  |");
  print("|<--+---+--+--+-|Z5-|---+-------|----+->|");
  print(" ---+--- --+-++- ---+--- ------- -------");
  print("|   |   |  |  | |   |__ |_Z3   |    |  |");
  print("|   |   |  |  | |  Z1   |  ^   |    |  |");
  print(" ---+--- --+--+- ---^--- --+---- ----+--");
  print("|   |   |  |  |_|___|___|__|____|____|  |");
  print("|   |   |  |    |   |   |       |       |");
  print(" ---+--- --V-+-- ---+--- ------- -------");
  print("|   |___|__Z4___|___|   |       |       |");
  print("|  YOU  |       |       |       |       |");
  print(" ------- ------- ------- ------- -------");
  List<String> str = [];
  str = ["How zombies walk :",
      "Z1 will walk like Rectangle(5*3). Start from (3,3)and walk to north first. ",
      "Z2 will walk like Rectangle(4*3). Start from (1,5)and walk to south first.",
      "Z3 will walk like Square(2*2). Start from (1,5)and walk to south first.",
      "Z4 will walk straight to north untill reah the last room. and turn back on the same way.",
      "Z5 will walk straight to west untill reah the last room. and turn back on the same way."];
      _runningDialog(str);

}

void zombiesAttack() {
   var dungeonMessage = new Runes('''
 \n 
__________________________________________________
__________________¶¶¶¶¶¶¶¶¶¶¶¶¶¶__________________
______________¶¶¶¶_____________¶¶¶¶¶______________
___________¶¶¶_____________________¶¶¶¶___________
________¶¶¶¶__________________________¶¶¶_________
_______¶¶_______________________________¶¶¶_______
______¶¶__________________________________¶¶______
____¶¶_____________________________________¶¶_____
____¶________________________________________¶____
___¶¶________________________________________¶¶___
__¶¶_____________¶¶¶__________________________¶___
___¶___________¶¶_____________________________¶¶__
___¶___________¶________________¶¶¶___________¶¶__
___¶_____¶¶_¶¶_¶¶¶¶¶_¶____________¶¶¶__________¶__
___¶_________¶___¶¶¶¶¶¶¶¶¶¶¶¶______¶¶¶_________¶__
___¶¶______¶¶¶¶___¶¶¶¶¶¶¶¶¶¶¶¶¶¶____¶¶¶_¶¶____¶¶¶_
___¶¶____¶¶¶¶¶¶___¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶__¶¶¶¶¶¶¶¶__¶¶__
___¶____¶¶¶¶¶¶¶____¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶__¶¶¶¶¶¶¶¶¶¶¶___
__¶¶___¶¶¶¶¶¶¶¶____¶¶¶¶¶¶¶¶¶¶¶¶¶¶__¶¶¶¶¶¶¶¶¶¶¶¶___
____¶¶¶¶¶¶¶¶¶¶______¶¶¶¶¶¶¶¶¶¶¶¶¶__¶¶¶¶¶¶¶¶¶¶¶____
______¶¶¶¶¶¶¶___¶¶_____¶¶¶¶¶¶¶¶¶____¶¶¶¶¶¶¶¶¶¶____
______¶__¶¶¶____¶¶¶___________________¶¶¶¶¶¶¶_____
_____¶¶________¶¶¶¶¶¶__¶________________¶¶¶_______
_____¶¶______¶¶¶____¶¶¶¶______________¶¶¶¶________
______¶¶______¶_______¶¶_________¶¶¶¶¶¶¶¶¶________
_______¶¶¶¶¶_______________¶_¶¶¶¶¶¶¶¶¶¶¶¶¶________
___________¶¶¶____¶¶¶¶_¶¶_¶¶¶___¶¶¶¶¶___¶¶________
____________¶¶¶¶¶_¶__¶¶_¶_¶_¶____¶¶_____¶_________
_____________¶_____________¶¶_____¶_____¶_________
______________¶¶¶¶¶_¶¶¶¶¶¶________¶¶____¶_________
____________________¶¶¶¶¶¶_________¶____¶_________
_____________________¶¶_¶¶_________¶¶___¶¶________
______________________¶¶¶¶¶_________¶____¶________
_______________________¶¶¶¶¶_______¶¶____¶¶_______
_______________________¶¶¶¶¶______¶_¶_____¶_______
_______________________¶_¶¶¶_____¶__¶¶____¶_______
_______________________¶¶¶¶¶____¶¶__¶¶___¶________
_______________________¶¶¶¶¶__¶¶¶__¶¶___¶_________
________________________¶¶¶¶¶¶¶__¶¶¶___¶¶_________
_________________________¶¶_____¶¶¶____¶__________
____________________________¶¶¶¶______¶___________
______________________________¶¶_____¶¶___________
_______________________________¶¶¶¶¶¶¶____________
__________________________________________________

 ''');
  print(new String.fromCharCodes(dungeonMessage));
  print("ZOMBIES ATTACK!!!!!");

}
void showLose(){
    var dungeonMessage = new Runes('''
 \n 

██╗░░░██╗░█████╗░██╗░░░██╗  ██╗░░░░░░█████╗░░██████╗███████╗░░░░░░░░░
╚██╗░██╔╝██╔══██╗██║░░░██║  ██║░░░░░██╔══██╗██╔════╝██╔════╝░░░░░░░░░
░╚████╔╝░██║░░██║██║░░░██║  ██║░░░░░██║░░██║╚█████╗░█████╗░░░░░░░░░░░
░░╚██╔╝░░██║░░██║██║░░░██║  ██║░░░░░██║░░██║░╚═══██╗██╔══╝░░░░░░░░░░░
░░░██║░░░╚█████╔╝╚██████╔╝  ███████╗╚█████╔╝██████╔╝███████╗██╗██╗██╗
░░░╚═╝░░░░╚════╝░░╚═════╝░  ╚══════╝░╚════╝░╚═════╝░╚══════╝╚═╝╚═╝╚═╝
 ''');
   print(new String.fromCharCodes(dungeonMessage));
}
 void _runningDialog(List<String> dialog, {speedText = 6, waitText = 1000}) {
    for (String element in dialog) {
      for (int i = 0; i < element.length; i++) {
        stdout.write(element[i]);
        sleep(Duration(milliseconds: speedText));
      }
      sleep(Duration(milliseconds: waitText));
      print("");
    }
  }
