import 'dart:io';

import 'Exit.dart';
import 'Player.dart';
import 'Zombies.dart';
import 'Object.dart';

class TableMap {
  int row = 5;
  int col = 5;
  late Player player;
  late Zombies zombies;
  var objCount = 0;
  var object = List<Object>.filled(25, Object(0, 0, ""), growable: false);
  bool escapeed = false;
  bool alive = true;
  late Exit ex = Exit(0, 4, "E");
  TableMap(this.row, this.col);

  void addObject(Object obj) {
    object[objCount] = obj;
    objCount++;
  }

  void setplayer(Player player) {
    this.player = player;
    addObject(player);
  }

  void setZombie(Zombies zombie) {
    zombies = zombie;
    addObject(zombie);
  }

  void printSymbolOn(int x, int y) {
    String symbol = '-';
    for (int o = 0; o < objCount; o++) {
      if (object[o].isOn(x, y)) {
        symbol = object[o].getSymbol();
      }
    }
    stdout.write(symbol);
  }

  void showMap() {
    print("-----+-----+-----+-----+-----");
    for (int x = 0; x < row; x++) {
      if (zombies.getX() == x) {
        stdout.write("| ");
      } else {
        stdout.write("|  ");
      }
      for (int y = 0; y < col; y++) {
        printSymbolOn(x, y);
        if (zombies.getY() == y) {
          stdout.write(" | ");
        } else {
          stdout.write("  |  ");
        }
      }
      print("");
      print("-----+-----+-----+-----+-----");
    }
  }

  void showCell() {
    stdout.write("-");
  }

  void showObject(Object object) {
    stdout.write(object.getSymbol());
  }

  void showZombie() {
    stdout.write(zombies.getSymbol());
  }

  void showPlayer() {
    stdout.write(player.getSymbol());
  }

  bool inMap(int x, int y) {
    return (x >= 0 && x < row) && (y >= 0 && y < col);
  }

  bool isExit(int x, int y) {
    return ex.isOn(x, y);
  }


  bool isZombie(int x, int y) {
    for (int o = 0; o < objCount; o++) {
      if (object[o] is Zombies && object[o].isOn(x, y)) {
        return true;
      }
    }
    return false;
  }


}
