import 'dart:io';

import 'Object.dart';
import 'TableMap.dart';
import 'walkable.dart';

class Player extends Object implements walkable {
  int x = 0;
  int y = 0;
  late int bomb = 0;
  String symbol = "";
  String direction = "";
  var room = TableMap(5, 5);
  var countWalk = 0;
  var totalStep = 13;
  Player(this.x, this.y, this.symbol, this.room, this.bomb) : super(0, 0, 'Y');
  String setPlayer(String symbol) {
    return this.symbol = symbol;
  }

  @override
  @override
  bool walk(String direction) {
    switch (direction) {
      // North
      case 'w':
        if (walkN()) {
          return false;
        }
        break;
      //South
      case 's':
        if (walkS()) {
          return false;
        }
        break;
      // East
      case 'd':
        if (walkE()) {
          return false;
        }
        break;
      // West
      case 'a':
        if (walkW()) {
          return false;
        }
        break;
      default:
        return false;
    }

    checkExit();
    checkZombies();
    return true;
  }

  bool canWalk(int x, int y) {
    return room.inMap(x, y);
  }

  bool walkN() {
    if (canWalk(x - 1, y)) {
      x -= 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkS() {
    if (canWalk(x + 1, y)) {
      x += 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkE() {
    if (canWalk(x, y + 1)) {
      y += 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkW() {
    if (canWalk(x, y - 1)) {
      y -= 1;
    } else {
      return true;
    }
    return false;
  }

  void checkExit() {
    if (isOn(0, 4)) {
      room.escapeed = true;
    }
  }

  bool checkZombies() {
    if (room.isZombie(x, y)) {
      return room.alive = false;
    }
    return true;
  }
}
