import 'Object.dart';
import 'walkable.dart';

class Zombies extends Object implements walkable{
   int x = 0;
  int y = 0;
  String symbol = ""; 

   Zombies(this.x, this.y , this.symbol) : super(0, 0, 'Z');
   
     @override
      bool walk(String direction) {
    switch (direction) {
      //North
      case 'w':
        {
          x = x - 1;
          return false;
        }
        break;
      //South
      case 's':
        {
          x = x + 1;
          return false;
        }
        break;
      //Eest
      case 'd':
        {
          y = y + 1;
          return false;
        }
        break;
      //West
      case 'a':
        {
          y = y - 1;
          return false;
        }
        break;
      default:
        {
          return false;
        }
        break;
    }
  }
   
}
