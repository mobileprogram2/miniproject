import 'Object.dart';

class Exit extends Object {
  int x = 0;
  int y = 0;
  String symbol = "";
  Exit(this.x, this.y, this.symbol) : super(0, 0, 'E');
}
